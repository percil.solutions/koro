/*
   This file is a part of the koro project.

   Copyright 2019 percil solutions.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
package be.percil.koro.repositories.impl;

import be.percil.koro.entities.AbstractKoroJpaEntity;
import be.percil.koro.repositories.KoroJpaRepository;
import be.percil.koro.search.AbstractKoroSearchCriteria;
import com.querydsl.core.types.EntityPath;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Default implementation of the {@link KoroJpaRepository} interface.
 *
 * @param <ID> the identifier type.
 * @param <E>  the JPA entity type.
 * @param <SC> the search criteria type.
 * @author Olivier Perceval
 */
public abstract class AbstractKoroJpaRepositoryImpl<ID extends Serializable, E extends AbstractKoroJpaEntity<ID>, SC extends AbstractKoroSearchCriteria<ID>> extends QuerydslRepositorySupport implements KoroJpaRepository<ID, E, SC> {

    public AbstractKoroJpaRepositoryImpl(Class<?> domainClass) {
        super(domainClass);
    }

    /**
     * Returns the entity path needed for query building.
     *
     * @return the entity path.
     */
    public abstract EntityPath<E> entityPath();

    /**
     * Builds a query based on the provided search criteria.
     *
     * @param searchCriteria the search criteria.
     * @return the query.
     */
    public abstract JPQLQuery<E> buildSearchQuery(SC searchCriteria);

    @Override
    public Optional<E> findSingle(SC searchCriteria) {
        JPQLQuery<E> query = buildSearchQuery(searchCriteria);
        return query.fetchCount() == 0 ? Optional.empty() : Optional.of(query.fetchFirst());
    }

    @Override
    public List<E> findAll(SC searchCriteria) {
        JPQLQuery<E> query = buildSearchQuery(searchCriteria);
        return query.fetch();
    }

    @Override
    public Page<E> findAll(SC searchCriteria, Pageable pageable) {
        JPQLQuery<E> query = Objects.requireNonNull(getQuerydsl()).applyPagination(pageable, buildSearchQuery(searchCriteria));
        long count = query.fetchCount();
        return new PageImpl<>(query.fetch(), pageable, count);
    }

    @Override
    public Long count(SC searchCriteria) {
        JPQLQuery<E> query = buildSearchQuery(searchCriteria);
        return query.fetchCount();
    }
}
