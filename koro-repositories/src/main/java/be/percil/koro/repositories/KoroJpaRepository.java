/*
   This file is a part of the koro project.

   Copyright 2019 percil solutions.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
package be.percil.koro.repositories;

import be.percil.koro.entities.AbstractKoroJpaEntity;
import be.percil.koro.search.AbstractKoroSearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Custom JPA repository extension to deal with the concept of search criteria.
 *
 * @param <ID> the identifier type.
 * @param <E>  the JPA entity type.
 * @param <SC> the search criteria type.
 * @author Olivier Perceval
 */
public interface KoroJpaRepository<ID extends Serializable, E extends AbstractKoroJpaEntity<ID>, SC extends AbstractKoroSearchCriteria<ID>> {

    /**
     * Finds potentially the element matching the search criteria.
     *
     * @param searchCriteria the search criteria.
     * @return the element. Maybe.
     */
    Optional<E> findSingle(SC searchCriteria);

    /**
     * Finds the entities matching the provided search criteria.
     *
     * @param searchCriteria the criteria.
     * @return the entities.
     */
    List<E> findAll(SC searchCriteria);

    /**
     * Finds the entities matching the provided search criteria with pagination.
     *
     * @param searchCriteria the search criteria.
     * @param pageable       the pagination.
     * @return the paginated entities.
     */
    Page<E> findAll(SC searchCriteria, Pageable pageable);

    /**
     * Returns the amount of entities matching the provided search criteria.
     *
     * @param searchCriteria the search criteria.
     * @return the amount of entities.
     */
    Long count(SC searchCriteria);
}
