/*
   This file is a part of the koro project.

   Copyright 2019 percil solutions.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
package be.percil.koro.services;

import be.percil.koro.bo.AbstractKoroBO;
import be.percil.koro.exceptions.KoroException;
import be.percil.koro.search.AbstractKoroSearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Interface defining the behaviour of a CRUD service for the specified business object.
 *
 * @param <ID> the identifier type.
 * @param <BO> the business object type.
 * @param <SC> the search criteria type.
 * @author Olivier Perceval
 */
public interface KoroService<ID extends Serializable, BO extends AbstractKoroBO<ID>, SC extends AbstractKoroSearchCriteria<ID>> {

    /**
     * Finds the business object having the provided identifier.
     *
     * @param id the identifier.
     * @return the business object.
     */
    Optional<BO> findOne(ID id);

    /**
     * Returns all the business objects.
     *
     * @return the business objects.
     */
    List<BO> list();

    /**
     * Returns all the business objects with pagination.
     *
     * @param pageable the pagination information.
     * @return the paginated business objects.
     */
    Page<BO> list(Pageable pageable);

    /**
     * Returns all the business objects matching the provided search criteria.
     *
     * @param searchCriteria the search criteria.
     * @return the business objects.
     * @throws KoroException is thrown if something goes wrong.
     */
    List<BO> findByCriteria(SC searchCriteria) throws KoroException;

    /**
     * Returns all the business objects matching the provided search criteria with pagination.
     *
     * @param searchCriteria the search criteria.
     * @param pageable       the pagination information.
     * @return the paginated business objects.
     * @throws KoroException is thrown if something goes wrong.
     */
    Page<BO> findByCriteria(SC searchCriteria, Pageable pageable) throws KoroException;

    /**
     * Returns the amount of items matching the provided search criteria.
     *
     * @param searchCriteria the search criteria.
     * @return the count.
     * @throws KoroException is thrown if something goes wrong.
     */
    Long countByCriteria(SC searchCriteria) throws KoroException;

    /**
     * Saves the provided business in the data store and returns the updated one.
     *
     * @param bo the business object to save.
     * @return the save business object.
     * @throws KoroException is thrown if something goes wrong.
     */
    BO save(BO bo) throws KoroException;

    /**
     * Deletes the provided business object from the data store and returns the identifier of the deleted BO.
     *
     * @param bo the business object.
     * @return the identifier of the deleted BO.
     * @throws KoroException is thrown if something goes wrong.
     */
    ID delete(BO bo) throws KoroException;

    /**
     * Validates the provided business object in order to save it.
     *
     * @param bo the business object to validate.
     * @throws KoroException is thrown if something goes wrong.
     */
    void validate(BO bo) throws KoroException;

    /**
     * Finds single the business object matching the provided search criteria.
     *
     * @param searchCriteria
     * @return
     */
    Optional<BO> findSingle(SC searchCriteria) throws KoroException;
}
