/*
   This file is a part of the koro project.

   Copyright 2019 percil solutions.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
package be.percil.koro.services.impl;

import be.percil.koro.bo.AbstractKoroBO;
import be.percil.koro.entities.AbstractKoroJpaEntity;
import be.percil.koro.exceptions.KoroException;
import be.percil.koro.mappers.KoroMapper;
import be.percil.koro.repositories.KoroJpaRepository;
import be.percil.koro.search.AbstractKoroSearchCriteria;
import be.percil.koro.services.KoroService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Abstract implementation of the {@link KoroService} interface.
 *
 * @param <ID> the identifier type.
 * @param <E>  the JPA entity type.
 * @param <BO> the business object type.
 * @param <M>  the mapper type.
 * @param <R>  the repository type.
 * @param <SC> the search criteria type.
 */
@Slf4j
public abstract class AbstractKoroServiceImpl<ID extends Serializable, E extends AbstractKoroJpaEntity<ID>, BO extends AbstractKoroBO<ID>, M extends KoroMapper<E, BO>, R extends JpaRepository<E, ID>, SC extends AbstractKoroSearchCriteria<ID>>
        implements KoroService<ID, BO, SC> {

    private final String supportedClass;

    protected AbstractKoroServiceImpl(Class<BO> supportedClass) {
        this.supportedClass = supportedClass.getSimpleName();
    }

    /**
     * Returns the mapper.
     *
     * @return the mapper.
     */
    protected abstract M mapper();

    /**
     * Returns the JPA repository.
     *
     * @return the repository.
     */
    protected abstract R repository();

    /**
     * Casts the repository to a {@link KoroJpaRepository} and returns it.
     *
     * @return the koro JPA repository.
     * @throws KoroException thrown if the JPA repository isn't an instance of {@link KoroJpaRepository}.
     */
    private KoroJpaRepository<ID, E, SC> koroJpaRepository() throws KoroException {
        if (repository() instanceof KoroJpaRepository) {
            return (KoroJpaRepository<ID, E, SC>) repository();
        } else {
            throw new KoroException("Unsupported repository", new UnsupportedOperationException());
        }
    }

    @Override
    public Optional<BO> findOne(ID id) {
        return repository().findById(id).map(mapper()::map);
    }

    @Override
    public List<BO> list() {
        return repository().findAll().stream()
                .map(mapper()::map)
                .collect(Collectors.toList());
    }

    @Override
    public Page<BO> list(Pageable pageable) {
        Assert.notNull(pageable, "The pageable cannot be null.");

        return repository().findAll(pageable).map(mapper()::map);
    }

    @Override
    public List<BO> findByCriteria(SC searchCriteria) throws KoroException {
        List<E> entities = koroJpaRepository().findAll(searchCriteria);
        return entities.stream()
                .map(mapper()::map)
                .collect(Collectors.toList());
    }

    @Override
    public Page<BO> findByCriteria(SC searchCriteria, Pageable pageable) throws KoroException {
        Assert.notNull(pageable, "The pageable cannot be null.");

        Page<E> entities = koroJpaRepository().findAll(searchCriteria, pageable);
        return entities.map(mapper()::map);

    }

    @Override
    public Long countByCriteria(SC searchCriteria) throws KoroException {
        return koroJpaRepository().count(searchCriteria);
    }

    @Override
    public BO save(BO bo) throws KoroException {
        validate(bo);
        log.debug("[save] About to save [{}]: {}.", supportedClass, bo.toString());

        Optional<E> sourceEntity = Optional.empty();
        // Checks if the BO already exists in the data store.
        if (bo.getId() != null)
            sourceEntity = repository().findById(bo.getId());

        try {
            // Maps the BO to a JPA entity.
            E entity = mapper().map(bo, sourceEntity);
            // Saves the entity.
            entity = repository().save(entity);
            // Let's inform the world.
            log.debug("[save] [{}]: {} with id {} was saved successfully.", supportedClass, bo, entity.getId());
            return mapper().map(entity);

        } catch (Exception e) {
            log.error("[save] Error while saving [{}]: {}\n\t{}", supportedClass, bo.toString(), e.getMessage());
            throw new KoroException("Error while saving a JPA entity.", e);
        }
    }

    @Override
    public ID delete(BO bo) throws KoroException {
        Assert.notNull(bo, "The business object cannot be null.");
        Assert.notNull(bo.getId(), "The identifier cannot be null.");

        ID id = bo.getId();
        try {
            repository().deleteById(id);
            return id;
        } catch (Exception e) {
            log.error("[delete] Error while deleting the entity {}: {}.", id, e.getMessage());
            throw new KoroException("Error while deleting a JPA entity", e);
        }
    }

    @Override
    public Optional<BO> findSingle(SC searchCriteria) throws KoroException {
        return koroJpaRepository().findSingle(searchCriteria).map(mapper()::map);
    }
}
