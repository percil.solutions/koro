package be.percil.koro.web.controllers;

import be.percil.koro.bo.AbstractKoroBO;
import be.percil.koro.exceptions.KoroException;
import be.percil.koro.search.AbstractKoroSearchCriteria;
import be.percil.koro.services.KoroService;
import be.percil.koro.web.forms.AbstractKoroForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

/**
 * Abstract controller for CRUD operations.
 *
 * @param <ID> the identifier type.
 * @param <BO> the business object type.
 * @param <SC> the search criteria type.
 * @param <F>  the web form type.
 * @param <S>  the service type.
 * @author Olivier Perceval
 */
@Slf4j
public abstract class AbstractKoroController<ID extends Serializable, BO extends AbstractKoroBO<ID>, SC extends AbstractKoroSearchCriteria<ID>, F extends AbstractKoroForm<ID, BO>, S extends KoroService<ID, BO, SC>> {

    public static final String FORM_ATTRIBUTE = "form";

    /**
     * Returns the service.
     *
     * @return the service.
     */
    protected abstract S service();

    /**
     * Creates a new web form.
     *
     * @return the web form.
     */
    protected abstract F newForm();

    /**
     * Enriches the provided form.
     *
     * @param form the form.
     * @return the enriched form.
     */
    protected abstract F enrichForm(F form);

    /**
     * Enriches the provided model.
     *
     * @param model the model.
     */
    protected abstract void enrichModel(Model model);

    /**
     * Returns the name of the list view.
     *
     * @return the view name.
     */
    protected abstract String listViewName();

    /**
     * Returns the name of the edit view.
     *
     * @return the view name.
     */
    protected abstract String editViewName();

    /**
     * Fills in the model with the provided form and enriches it.
     *
     * @param model the model.
     * @param form  the form.
     */
    private void fillModel(Model model, F form) {
        model.addAttribute(FORM_ATTRIBUTE, enrichForm(form));
        enrichModel(model);
    }

    /**
     * Creates the list view and fills the model with the right form.
     *
     * @param model the model.
     * @return the list view name.
     */
    @GetMapping({"", "/list"})
    public String listAll(Model model) {
        F form = newForm();
        form.setItems(service().list());

        fillModel(model, form);

        return listViewName();
    }

    /**
     * Creates the details view for a new item and fills the model with the right form.
     *
     * @param model the model.
     * @return the edit view name.
     */
    @GetMapping("/add")
    public String add(Model model) {
        F form = newForm();
        fillModel(model, form);
        return editViewName();
    }

    /**
     * Creates the details view and fills the model with the right form.
     *
     * @param id    the identifier of the item.
     * @param model the model.
     * @return the edit view name.
     * @throws KoroException thrown if the no such item were found.
     */
    @GetMapping({"/{id}", "/{id}/details"})
    public String details(@PathVariable ID id, Model model) throws KoroException {
        F form = newForm();
        form.setItem(service().findOne(id).orElseThrow(() -> new KoroException("No item found for id " + id)));

        fillModel(model, form);

        return editViewName();
    }

    /**
     * Saves the item of the provided form.
     *
     * @param form the form.
     * @return the saved item.
     * @throws KoroException thrown if the save fails.
     */
    @PostMapping("/save")
    @ResponseBody
    public ResponseEntity<BO> save(@ModelAttribute F form) throws KoroException {
        return ResponseEntity.ok(service().save(form.getItem()));
    }

    /**
     * Deletes the item having the provided identifier.
     *
     * @param id the identifier.
     * @return the list view.
     * @throws KoroException thrown if the no such item were found.
     */
    @GetMapping("/{id}/delete")
    public String delete(@PathVariable ID id) throws KoroException {
        BO item = service().findOne(id).orElseThrow(() -> new KoroException("No item found for id " + id));
        service().delete(item);
        return "redirect:" + listViewName();
    }
}
