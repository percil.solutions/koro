package be.percil.koro.web.forms;

import be.percil.koro.bo.AbstractKoroBO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Base class for web form.
 *
 * @param <ID> the identifier type.
 * @param <BO> the business object type.
 * @author Olivier Perceval
 */
@Data
public abstract class AbstractKoroForm<ID extends Serializable, BO extends AbstractKoroBO<ID>> {

    private BO item;
    private List<BO> items;
}
