package be.percil.koro.mappers;

import be.percil.koro.bo.AbstractKoroBO;
import be.percil.koro.entities.AbstractKoroJpaEntity;

import java.util.Optional;

/**
 * Default behaviour for a mapper between JPA entity and the related business object.
 *
 * @param <E>  the JPA entity type.
 * @param <BO> the business object type.
 * @author Olivier Perceval
 */
public interface KoroMapper<E extends AbstractKoroJpaEntity, BO extends AbstractKoroBO> {

    /**
     * Maps the provided entity into a business object.
     *
     * @param entity the entity.
     * @return the business object.
     */
    BO map(E entity);

    /**
     * Maps the provided business object in the provided source entity. If no such entity is present, a new one is created.
     *
     * @param bo           the business object.
     * @param sourceEntity the source entity. Maybe.
     * @return the mapped entity.
     */
    E map(BO bo, Optional<E> sourceEntity);
}
