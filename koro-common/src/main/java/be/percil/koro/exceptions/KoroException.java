package be.percil.koro.exceptions;

/**
 * Exception thrown when a validation error occurs in the abstract level of koro.
 *
 * @author Olivier Perceval
 */
public class KoroException extends Exception {

    public KoroException() {
    }

    public KoroException(String message) {
        super(message);
    }

    public KoroException(String message, Throwable cause) {
        super(message, cause);
    }

    public KoroException(Throwable cause) {
        super(cause);
    }

    public KoroException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
