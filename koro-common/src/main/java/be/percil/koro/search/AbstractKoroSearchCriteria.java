/*
   This file is a part of the koro project.

   Copyright 2019 percil solutions.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
package be.percil.koro.search;

import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/**
 * Base class for search criteria.
 *
 * @param <ID> the identifier type.
 * @author Olivier Perceval
 */
@Getter
public abstract class AbstractKoroSearchCriteria<ID extends Serializable> {

    private ID id;
    private List<ID> ids;

    public AbstractKoroSearchCriteria(ID id, List<ID> ids) {
        this.id = id;
        this.ids = ids;
    }
}
