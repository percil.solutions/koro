package be.percil.koro.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * Base class for business objects.
 *
 * @param <ID> the identifier type.
 * @author Olivier Perceval
 */
@Data
public abstract class AbstractKoroBO<ID extends Serializable> {

    /**
     * The identifier.
     */
    private ID id;
}
